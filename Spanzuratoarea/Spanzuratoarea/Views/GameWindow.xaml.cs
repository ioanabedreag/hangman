﻿using Spanzuratoarea.Views;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Spanzuratoarea
{
    public partial class GameWindow : Window
    {

        private List<TextBlock> MistakeBoxes { get; set; }
        private Dictionary<char, Button> LetterButtons { get; set; }

        public GameWindow()
        {
            InitializeComponent();

            if (!Util.Initialized)
                Util.Initialize();

            InitializeLettersMistakes();

            Util.NewGame();
            InitializeGameScreen();

        }

        private void InitializeLettersMistakes()
        {
            LetterButtons = new Dictionary<char, Button>();
            LetterButtons['A'] = btnA;
            LetterButtons['B'] = btnB;
            LetterButtons['C'] = btnC;
            LetterButtons['D'] = btnD;
            LetterButtons['E'] = btnE;
            LetterButtons['F'] = btnF;
            LetterButtons['G'] = btnG;
            LetterButtons['H'] = btnH;
            LetterButtons['I'] = btnI;
            LetterButtons['J'] = btnJ;
            LetterButtons['K'] = btnK;
            LetterButtons['L'] = btnL;
            LetterButtons['M'] = btnM;
            LetterButtons['N'] = btnN;
            LetterButtons['O'] = btnO;
            LetterButtons['P'] = btnP;
            LetterButtons['Q'] = btnQ;
            LetterButtons['R'] = btnR;
            LetterButtons['S'] = btnS;
            LetterButtons['T'] = btnT;
            LetterButtons['U'] = btnU;
            LetterButtons['V'] = btnV;
            LetterButtons['X'] = btnX;
            LetterButtons['Y'] = btnY;
            LetterButtons['Z'] = btnZ;
            LetterButtons['W'] = btnW;

            MistakeBoxes = new List<TextBlock>();
            MistakeBoxes.Add(txtMistake1);
            MistakeBoxes.Add(txtMistake2);
            MistakeBoxes.Add(txtMistake3);
            MistakeBoxes.Add(txtMistake4);
            MistakeBoxes.Add(txtMistake5);
            MistakeBoxes.Add(txtMistake6);
        }

        private void InitializeGameScreen()
        {
            ResetLetterButtons();
            ResetMistakeBoxes();
            UpdateScreen();
        }

        private void Letter_Button_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            Util.Game.TryLetter(button.Content.ToString()[0]);
            UpdateScreen();
            CheckGameState();
            button.IsEnabled = false;
        }

        private void UpdateScreen()
        {
            img_stage.Source = Util.GetStageImage();
            UpdateMistakeBoxes();
            txtWord.Text = Util.Game.GetDisplayedText();
        }

        private void ResetMistakeBoxes()
        {
            foreach (TextBlock box in MistakeBoxes)
                box.Text = "";
        }

        private void UpdateMistakeBoxes()
        {
            int failedAttempts = Util.Game.FailedAttempts;

            if (failedAttempts > 0)
            {
                for (int i = 0; i < failedAttempts; ++i)
                    MistakeBoxes[i].Text = "X";
            }
        }

        private void ResetLetterButtons()
        {
            string attempts = Util.Game.Attempts;
            if (attempts == "")
            {
                foreach (KeyValuePair<char, Button> button in LetterButtons)
                {
                    button.Value.IsEnabled = true;
                }
            }
            else
            {
                foreach (KeyValuePair<char, Button> button in LetterButtons)
                {
                    if (attempts.Contains(button.Key))
                        button.Value.IsEnabled = false;
                    else
                        button.Value.IsEnabled = true;
                }
            }
        }

        private void DisableAllLetterButtons()
        {
            foreach (KeyValuePair<char, Button> button in LetterButtons)
            {
                button.Value.IsEnabled = false;
            }
        }

        private void CheckGameState()
        {
            switch (Util.GetGameState())
            {
                case Game.GameState.Won:
                    {
                        MessageBox.Show("You won.", "Game state", MessageBoxButton.OK);
                        DisableAllLetterButtons();
                        break;
                    }
                case Game.GameState.Lost:
                    {
                        MessageBox.Show($"You lost.\nWord: {Util.Game.Word}.", "Game state", MessageBoxButton.OK);
                        DisableAllLetterButtons();
                        break;
                    }
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnNewGame_Click(object sender, RoutedEventArgs e)
        {
            Util.NewGame();
            InitializeGameScreen();
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            Window window = new HelpWindow();
            window.Show();

            Close();
        }
    }
}
