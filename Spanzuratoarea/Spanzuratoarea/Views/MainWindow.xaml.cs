﻿using Spanzuratoarea.Views;
using System.Windows;

namespace Spanzuratoarea
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            Window window = new UserWindow();
            window.Show();

            Close();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            Window window = new HelpWindow();
            window.Show();

            Close();
        }
    }
}
