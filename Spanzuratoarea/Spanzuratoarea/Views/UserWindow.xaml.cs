﻿using Spanzuratoarea.Models;
using Spanzuratoarea.Utils;
using System;
using System.Collections.Generic;
using System.Windows;

namespace Spanzuratoarea.Views
{

    public partial class UserWindow : Window
    {
        List<User> users;
        Operations operation;

        public UserWindow()
        {
            InitializeComponent();

            operation = new Operations();
            users = operation.GetUserList();
            PrintUserList();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (IsValidUser(txtNewUsername.Text))
            {
                User newUser = new User(txtNewUsername.Text);
                operation.AddUser(newUser);
            }

            txtNewUsername.Text = "";
            operation.UpdateUserData();
            PrintUserList();
        }

        private void btnDeleteUser_Click(object sender, RoutedEventArgs e)
        {
            if (listUsers.SelectedItem != null)
            {
                for (int index = 0; index < users.Count; index++)
                {
                    if (users[index].GetName() == listUsers.SelectedItem.ToString())
                    {
                        operation.RemoveUser(users[index]);
                    }
                }
            }
            PrintUserList();
            operation.UpdateUserData();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Window window = new MainWindow();
            window.Show();

            Close();
        }

        public bool IsValidUser(string username)
        {
            for (int index = 0; index < users.Count; index++)
            {
                if (users[index].GetName() == username)
                {
                    MessageBox.Show($"User already exists!\n", "ERROR!", MessageBoxButton.OK);
                    return false;
                }
            }

            return true;
        }

        public void PrintUserList()
        {
            listUsers.Items.Clear();
            for (int index = 0; index < users.Count; index++)
            {
                listUsers.Items.Add(users[index].GetName());
            }
        }

        private void btnStartGame_Click(object sender, RoutedEventArgs e)
        {
            if (listUsers.SelectedItem != null)
            {
                Window window = new GameWindow();
                window.Show();

                Close();
            }
            else
            {
                MessageBox.Show($"Please select a user!!\n", "ERROR!", MessageBoxButton.OK);
            }
        }
    }
}
