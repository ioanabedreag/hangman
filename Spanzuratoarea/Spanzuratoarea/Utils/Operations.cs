﻿using Spanzuratoarea.Models;
using System.Collections.Generic;
using System.IO;

namespace Spanzuratoarea.Utils
{
    class Operations
    {
        private List<User> users;
        private string UsersFilePath = "../../users.txt";

        public Operations()
        {
            RetrieveUserData();
        }

        public List<User> GetUserList()
        {
            return users;
        }

        public void AddUser(User user)
        {
            users.Add(user);
        }

        public void RemoveUser(User user)
        {
            users.Remove(user);
        }

        public User FindUserByName(string username)
        {
            for (int index = 0; index < users.Count; index++)
            {
                if (users[index].GetName() == username)
                    return users[index];
            }
            return null;
        }

        public void RetrieveUserData()
        {
            StreamReader sr = new StreamReader(UsersFilePath);
            string username = sr.ReadLine();
            users = new List<User>(0);

            while (username != null)
            {
                User newUser = new User(username);
                users.Add(newUser);
                username = sr.ReadLine();
            }

            sr.Close();
        }

        public void UpdateUserData()
        {
            StreamWriter sw = new StreamWriter(UsersFilePath);

            for (int index = 0; index < users.Count; index++)
            {
                sw.WriteLine(users[index].GetName());
            }

            sw.Close();
        }
    }
}
