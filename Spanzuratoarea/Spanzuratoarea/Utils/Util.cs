﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace Spanzuratoarea
{
    class Util
    {
        public static Game Game { get; set; }

        private static string WordsFilesPath { get; set; }
        private static List<string> Words { get; set; }
        public static uint Score { get; set; }

        private static List<BitmapImage> StageImages { get; set; }
        private static string ImagesPath { get; set; }

        public static bool Initialized { get; private set; }


        public static void Initialize()
        {
            //profile pics
            ImagesPath = "..\\..\\Resources\\GameStages";
            InitializeImages();

            // words
            WordsFilesPath = "..\\..\\Resources\\Words";
            InitializeWords();

            Initialized = true;
        }

        private static void InitializeImages()
        {
            StageImages = new List<BitmapImage>();

            int maxStages = 7;

            for (int i = 0; i < maxStages; ++i)
            {
                StageImages.Add(new BitmapImage(new Uri(ImagesPath + $"\\stage{i}.png", UriKind.Relative)));
            }
        }

        private static void InitializeWords()
        {
            Words = new List<string>();

            ReadList(Words, WordsFilesPath + "\\words.txt");
        }


        private static void ReadList(List<string> list, string path)
        {
            string[] lines = System.IO.File.ReadAllLines(path);

            foreach (string line in lines)
                list.Add(line.ToUpper());
        }


        private static string GenerateWord()
        {
            Random rand = new Random();
            return Words[rand.Next(0, Words.Count)];

        }


        public static void NewGame()
        {
            Game = new Game(GenerateWord());
        }

        public static BitmapImage GetStageImage()
        {
            return StageImages[Game.FailedAttempts];
        }

        public static Game.GameState GetGameState()
        {
            return Game.GetGameState();
        }

    }
}
