﻿
namespace Spanzuratoarea.Models
{
    class User
    {
        private string name;

        public User()
        {
            this.name = null;
        }

        public User(string name)
        {
            this.name = name;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public string GetName()
        {
            return name;
        }

    }
}
