﻿using System;
using System.Linq;

namespace Spanzuratoarea
{
    class Game
    {
        public enum GameState
        {
            Won,
            Ongoing,
            Lost
        }

        public string Word { get; set; }

        public string Attempts { get; set; }

        public int FailedAttempts { get; private set; }

        public Game(string word)
        {
            Word = word;
            Attempts = "";
            FailedAttempts = 0;
        }

        public void TryLetter(char c)
        {
            Attempts += c;

            if (!Word.Contains(c))
                FailedAttempts++;
        }

        public string GetDisplayedText()
        {
            string text = "";

            foreach (char c in Word)
            {
                if (Char.IsLetter(c))
                {
                    if (Attempts.Contains(c))
                    {
                        text += c + " ";
                    }
                    else
                    {
                        text += "_ ";
                    }
                }
                else
                {
                    text += c + " ";
                }
            }

            return text;
        }

        public GameState GetGameState()
        {
            if (CheckForWin())
                return GameState.Won;

            if (FailedAttempts == 6)
                return GameState.Lost;

            return GameState.Ongoing;
        }

        private bool CheckForWin()
        {
            foreach (char c in Word)
            {
                if (Char.IsLetter(c) && !Attempts.Contains(c))
                    return false;
            }

            return true;
        }
    }
}
